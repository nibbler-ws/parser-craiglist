package main

import (
	_ "github.com/gizak/termui"
	)

// start point
func main() {
	a := App{}
	a.Initialize("")
	a.Run(":8881")
}