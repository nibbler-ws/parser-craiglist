package main

import (
	"github.com/gorilla/websocket"
	"github.com/PuerkitoBio/goquery"
	"github.com/meirf/gopart"
	"fmt"
	"log"
	"strconv"
	"runtime"
	"sync"
	"time"
)

// response struct data
type BrowserResponse struct {
	Status string `json:"status"`
	Url    string `json:"url"`
	Name   string `json:"name"`
	Email  string `json:"email,omitempty"`
	Title  string `json:"title"`
}

// function send data to socket connection
func WriteWebSocket(ws *websocket.Conn, list *LinksColcection, workers int, timelimit time.Duration, wsync *sync.WaitGroup) {
	var aw sync.WaitGroup
	mu := &sync.Mutex{}

	var err error
	ttl := strconv.Itoa(list.Total)
	err = ws.WriteMessage(websocket.TextMessage, []byte(ttl))
	if err != nil {
		log.Fatal(err)
	}

	thread := func(collection *LinksColcection, low int, hight int, mu *sync.Mutex, aw *sync.WaitGroup) {
		//itemCount := len(list.Links)
		fmt.Println("Gorutine run limit: ", strconv.Itoa(low), " : ", strconv.Itoa(hight))
		for i := low; i < hight; {
			mu.Lock()
				err = ws.WriteMessage(websocket.TextMessage, []byte(list.Links[i]))
			mu.Unlock()
			time.Sleep(timelimit * time.Second)
			i++
		}
		aw.Done()
		fmt.Println("Gorutine thread finish")
		fmt.Println(runtime.NumGoroutine())
	}

	countCollection := len(list.Links) / workers
	for idxRange := range gopart.Partition(len(list.Links), countCollection) {
		low := idxRange.Low
		hight := idxRange.High
		aw.Add(1)
		go thread(list, low, hight, mu, &aw)
	}
	runtime.Gosched()
	aw.Wait()
	wsync.Done()
	fmt.Println("Gorutine write finish")
	fmt.Println(runtime.NumGoroutine())
}

// response from websocket data
// link ws.Conn | count element to end
func ReadWebSocket(ws *websocket.Conn, c chan BrowserResponse, allCount int, wsync *sync.WaitGroup) {
	defer ws.Close()
	i := 0
	for {
		// если получили ответ парсим. сверять с Url структурой (для сопоставления по какому запросу ответ пришел)
		response := BrowserResponse{}
		if err := ws.ReadJSON(&response); err != nil {
			fmt.Println("Can't recived")
		} else {
			doc, _ := goquery.NewDocument(response.Url)
			title := doc.Find("#titletextonly").Text()
			response.Title = title
			c <- response
			fmt.Println(strconv.Itoa(i))
			fmt.Println(strconv.Itoa(allCount))
			if allCount > i {
				i++
			} else {
				break
			}
		}
	}
	fmt.Println("Gorutine read finish")
	fmt.Println(runtime.NumGoroutine())
	wsync.Done()
	close(c)
}
