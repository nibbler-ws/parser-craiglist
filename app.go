package main

import (
	"github.com/gorilla/websocket"
	"github.com/gorilla/mux"
	"encoding/json"
	"net/http"
	"sync"
	"time"
	"log"
	"fmt"
	"os"
	"runtime"
)

// application structure
type App struct {
	Router *mux.Router
	Config *Config
	conn   *websocket.Conn
}

//Confguration struct
type Config struct {
	Url           string
	Workers       int
	File_path     string
	Request_speed time.Duration
	StepCount     int
}

// initialize App struct
// config path for application
func (a *App) Initialize(config_path string) {
	file, err := os.Open("config.json")
	defer file.Close()

	if err != nil {
		log.Fatal("not find config.json")
		file.Close()
		return
	}
	//decode cofig
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&a.Config)
	if err != nil {
		log.Fatalln("File read error")
	}
	a.Router = mux.NewRouter()
	a.initializeRoutes()
}

// run method start application on the port
func (a *App) Run(port string) {
	log.Fatal(http.ListenAndServe(port, a.Router))
}

// initialize routes
func (a *App) initializeRoutes() {
	a.Router.HandleFunc("/", a.WebSocket)
}

///// handlers routes
func (a *App) WebSocket(w http.ResponseWriter, r *http.Request) {
	//sync
	var wg sync.WaitGroup

	// get list items for email parse
	list := Start(a.Config.Url, a.Config.StepCount)
	if len(list.Links) == 0 {
		log.Fatal("empty links items for scrapping")
	}

	file := Initialize(a.Config.File_path)
	if err := file.Open(a.Config.File_path); err != nil {
		log.Fatal(err.Error())
	}

	a.conn = a.socketConnection(w, r)
	defer a.conn.Close()

	// create map channel for sync
	mapChannel := make(chan BrowserResponse)

	wg.Add(1)
	go WriteWebSocket(a.conn, list, a.Config.Workers, a.Config.Request_speed, &wg)

	wg.Add(1)
	go ReadWebSocket(a.conn, mapChannel, list.Total, &wg)

	wg.Add(1)
	go MapEmail(mapChannel, file, &wg)

	wg.Wait()
	fmt.Println("All gorutines done")
}

func (a *App) socketConnection(w http.ResponseWriter, r *http.Request) (conn *websocket.Conn){
	upgrader := websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		CheckOrigin: func(r *http.Request) bool {return true},
	}

	var err error
	conn, err = upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Fatal(err)
	}

	return conn
}

/// write results to xlsx
func MapEmail(mapchan chan BrowserResponse, file *ExcelStruct, wg *sync.WaitGroup) {

	last := file.LastElement()
	for {
		response, ok := <- mapchan
		if !ok {
			fmt.Println("map done, close")
			wg.Done()
			fmt.Println("Gorutine Map finish")
			fmt.Println(runtime.NumGoroutine())
			break
		}
		fmt.Println(response)
		if result, _ := file.SearchHash(response.Email); result == false {
			file.AddToHash(response.Email)
			if err := file.ReciveData(response.Email, response.Title, response.Url, response.Name, response.Status, last); err != nil{
				fmt.Println(err)
			}
			last++
		}
	}

}
